import {html, css } from 'lit-element';
import {TechUBaseElement} from './base-component.js';
import './ticker.js';

class UserLogin extends TechUBaseElement {
  constructor(){
    super();
    // define empty user info
    this.userInfo = this._getEmptyUser();
    this.jwt=null;
  }

  static get styles() {
    return css`

        #loginPanel{
          height: 100%;
          align-content: center;
        }

        .card-header{
            background-color: darkblue;
            color:white;
        }

        .techu_logo{
        }

        .techu_logo img{
          height: 2.5rem;
          width: 2.5rem;
        }

        #loginCard{
          margin-top: auto;
          margin-bottom: auto;
        }

        #loginPanel .links a{
          margin-left: 4px;
        }

        .login_btn{
          background-color: darkblue;
          color:white;
        }

        .techu-header{
          background-color: darkblue;
          color:white;
        }

        .techu-header img{
          height: 2rem;
          width: 2rem;
        }

        .logout-icon {
          font-size: 2rem;
          padding:0px;
          margin:0px;
          color:white;
        }

        @media (min-width: 768px){ /* IPad */

          .techu-header img{
            height: 3rem;
            width: 3rem;
          }
          .logout-icon {
            font-size: 3rem;
            padding-top:0.5rem;
          }
          .techu-header{
            font-size: 2rem;
          }

        }
    `;
  }


  render() {
    return html`

      ${this.commonStyleSheets}
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

      <div id="loginPanel">
        <div class="d-flex justify-content-center h-100">
          <div class="container" id="loginCard">
            <div class="row d-flex justify-content-center">
        		<div class="card col-12 col-sm-7 col-md-5">
              <div class="card-header row">
        				<h3 class="col-9 align-items-center">Sign In</h3>
                <div class="col-3 techu_logo d-flex justify-content-end align-items-center">
                  <img src="img/TechUBank-logo.png"/><span>TechU Bank</span>
                </div>
        			</div>
        			<div class="card-body">
      					<div class="input-group form-group">
      						<div class="input-group-prepend">
      							<span class="input-group-text"><i class="fas fa-user"></i></span>
      						</div>
                  <input class="form-control" type="email" placeholder="email" required="true"
                      .value="${this.userInfo.email}" @change="${(e)=>this.userInfo.email=e.target.value}"/>
      					</div>
      					<div class="input-group form-group">
      						<div class="input-group-prepend">
      							<span class="input-group-text"><i class="fas fa-key"></i></span>
      						</div>
                  <input type="password" class="form-control" placeholder="password"
                      .value="${this.userInfo.password}" @change="${(e)=>this.userInfo.password=e.target.value}"/>
      					</div>
                <div id="registry" style="display:none">
                  <div class="input-group form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-user"></i></span>
                    </div>
                    <input class="form-control" type="text" placeholder="name" required="true"
                        .value="${this.userInfo.firstName}" @change="${(e)=>this.userInfo.firstName=e.target.value}"/>
                  </div>
                  <div class="input-group form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-user"></i></span>
                    </div>
                    <input class="form-control" type="text" placeholder="last name" required="true"
                        .value="${this.userInfo.lastName}" @change="${(e)=>this.userInfo.lastName=e.target.value}"/>
                  </div>
                  <div class="form-group">
                    <input type="submit" value="Register" class="btn float-right login_btn" @click="${this._register}">
                    <div class="d-flex justify-content-center links">
            					<a href="#" @click="${this._backToLogin}">Back to Login</a>
            				</div>
                  </div>
                </div>
      					<div class="form-group" id="loginFormGroup">
                  <input type="submit" value="Login" class="btn float-right login_btn" @click="${this._login}">
      					</div>
        			</div>
        			<div class="card-footer" id="changeToRegistry">
        				<div class="d-flex justify-content-center links">
        					Don't have an account?<a href="#" @click="${this._changeToRegistry}">Sign Up</a>
        				</div>
        			</div>
              <div class="card-footer">
              </div>
        		</div>
            </div> <!-- card row -->
            <div class="row d-flex justify-content-center">
              <div class="techu-msg">${this.msg}</div>
              <div class="techu-err">${this.errorMsg}</div>
            </div>
        	</div>
        </div>
      </div> <!-- loginPanel -->
      <!-- LOGOUT PANEL -->
      <div id="logoutPanel" style="display:none">
        <!-- NAV BAR -->
        <nav class="navbar techu-header">
      		<a class="navbar-brand" href="#">
            <img src="img/TechUBank-logo.png"/>
          </a>
          <div class="navbar-text">
            <span>Hello </span>
            <span id="username">
              ${this.userInfo.firstName+" "+this.userInfo.lastName}
            </span>
          </div>
          <div class="navbar-text">
            <a href="#">
              <i class="logout-icon material-icons" @click="${this._logout}">exit_to_app</i>
            </a>
          </div>
        </nav>
        <!-- NAV BAR -->
        <!-- TICKER -->
        <techu-ticker></techu-ticker>
        <!-- TICKER -->
        </div>
        <!-- LOGOUT PANEL -->
    `;
  }
  static get properties() {
    return {
      userInfo:{type:Object},
      jwt:{type:String}
    };
  }

  _getEmptyUser(){
    return {
      email:null,
      password:null,
      firstName:null,
      lastName:null
    };
  }

  _changeToRegistry(){
    this.$('#registry').style.display="block";
    this.$('#changeToRegistry').style.display="none";
    this.$('#loginFormGroup').style.display="none";
  }

  _backToLogin(){
    this.$('#registry').style.display="none";
    this.$('#changeToRegistry').style.display="block";
    this.$('#loginFormGroup').style.display="block";
  }

  _register(){
      // remove previous messages
      this.resetMsgs();
      this.bc.go('POST','techubank/v1/users',this.userInfo,
          this._handleSucessfulRegister.bind(this),
          this._handleError.bind(this));
  }

  _handleSucessfulRegister(resp){
    this.msg="User "+this.userInfo.email+" successfully registered";
  }


  _login(){
    // remove previous messages
    this.resetMsgs();
    this.bc.go('POST','techubank/v1/sessions',this.userInfo,
        this._handleSucessfulLogin.bind(this),
        this._handleError.bind(this));
  }

  _handleSucessfulLogin(resp){
    // set user info (gathered from server)
    this.userInfo.firstName=resp.firstName;
    this.userInfo.lastName=resp.lastName;
    // set JWT for further operations
    this.jwt=resp.jwt;
    // hide login panel and show logout loginPanel
    this.$('#loginPanel').style.display="none";
    this.$('#logoutPanel').style.display="block";
    //this.$('#username').innerHTML=this.userInfo.firstName+" "+this.userInfo.lastName;
    console.log(this.userInfo);
    // send login event to main panel
    this.dispatchEvent(
      new CustomEvent(
    	"loginevent",
    	{
    	  "detail":{
    		"loggedUser":this.userInfo.email,
    		"jwt":this.jwt
    	  }
    	}
    ));
  }

  _logout(){
    console.log("login out");
    // reset user info
    this.userInfo=this._getEmptyUser();
    // reset JWT
    this.jwt=null;
    // hide logout panel and show login panel
    this.$('#loginPanel').style.display="block";
    this.$('#logoutPanel').style.display="none";
    //this.$('#username').innerHTML="";

    // send logout event to main panel
    this.dispatchEvent(
      new CustomEvent(
    	"logoutevent",
    	{
    	  "detail":{
          // no need for details
    	  }
    	}
    ));
  }

}

window.customElements.define('user-login', UserLogin);
