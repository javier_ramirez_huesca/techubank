import {html, css } from 'lit-element';
import {TechUBaseElement} from './base-component.js';
import '@polymer/paper-dialog/paper-dialog.js';

class StockDialog extends TechUBaseElement {

  static get styles() {
    return css`
        #stockDlg button i{
          color:darkblue;
        }

        @media (min-width: 768px){ /* IPad */
          #stockDlg button i{
            font-size:1.5rem;
          }
        }
    `;
  }

  render(){
    return html`
    ${this.commonStyleSheets}
    <!-- STOCK DIALOG -->
    <paper-dialog id="stockDlg" modal>
      <div class="dialog-body">
        <h2>Select stocks</h2>
        <div class="input-group form-group">
          <input id="company" class="form-control" type="text" placeholder="company symbol"/>
          <button type="button" @click="${this._searchStock}" class="input-group-prepend">
            <i class="fas fa-search-dollar"></i>
          </button>
        </div>
        <div class="form-group row">
          <label for="price" class="col-3 col-form-label">Price:</label>
          <div class="col-12">
                <input id="price" type="text" readonly class="form-control-plaintext"/>
          </div>
        </div>
        <div class="input-group form-group row">
          <label for="shareAmount" class="col-3 col-form-label">Amount</label>
          <div class="col-12">
            <input id="shareAmount" class="form-control" type="text" placeholder="share amount"/>
          </div>
        </div>
      </div>
      <div class="d-flex justify-content-between dlg-buttons">
        <button class="btn btn-sm" dialog-dismiss>Cancel</button>
        <button @click="${this._doBuyShares}" class="btn btn-sm" autofocus>Accept</button>
      </div>
    </paper-dialog>
    <!-- STOCK DIALOG -->
    `;
  }

  static get properties() {
    return {
      activeAccountID:{type:String,attribute:false}
    };
  }

  constructor() {
    super();
    this.activeAccountID=null;
  }

  open(){
    this.$('#stockDlg').open();
  }

  _searchStock(){
    console.log("_searchStock");
    // invoke to financialmodelingprep.com to gaher company info and price
    let symbol = this.$('#company').value;
    let companyNameElm = this.$('#companyName');
    let priceElm = this.$('#price');

    console.log("Looking for price of "+symbol);
    let xhr = new XMLHttpRequest();
    xhr.open('GET', "https://financialmodelingprep.com/api/company/real-time-price/"+symbol+"?datatype=json", true);
    xhr.onreadystatechange=function(){
      if (this.readyState == 4) {
        let resp = {};
        if(this.responseText!=null && this.responseText!=''){
          // assume response is a json
          resp = JSON.parse(this.responseText);
        }
        //resp.status = this.status;
        if(this.status == 200 || this.status == 201){
          console.log("Info for "+symbol+" returned by financialmodelingprep");
          console.log(resp);
          priceElm.value=resp.price.toLocaleString(window.language,{"style":"currency","currency":"EUR"});
        }else{
          console.log("Error invoking callback:");
          console.log(resp);
        }
      }
    };
    xhr.send(null);
  }

  _doBuyShares(){
    // create a transaction without target and source selected account
    // and ammount = (price*amount)
    console.log("_doBuyShares");
    this.$('#stockDlg').close();
    let newTX = {
      "source":this.activeAccountID,
      "amount":parseInt(this.$('#shareAmount').value,10)
                *parseFloat(this.$('#price').value.replace(/,/gi,'.'),10)
    };
    this.bc.go('POST',"techubank/v1/users/"+this.context.loggedUser+"/accounts/"+this.activeAccountID+"/transactions",newTX,
              this._handleSucessfulPOSTTransaction.bind(this),
              this._handleError.bind(this));
  }

  _handleSucessfulPOSTTransaction(resp){
    // emit event for accounts info modified (not only tx)
    this.dispatchEvent(
      new CustomEvent(
        'newtransaction',
      	{
      	  detail:{},
          bubbles: true,
          composed: true
      	}
    ));
  }


}
customElements.define('stock-dialog', StockDialog);
