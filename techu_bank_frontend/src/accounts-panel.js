import {html, css } from 'lit-element';
import {TechUBaseElement} from './base-component.js';
import './account-list.js';
import './account-detail.js';

class AccountsPanel extends TechUBaseElement {
  constructor(){
    super();
  }

  static get styles() {
    return css`
      #accountsPanel{
        margin-top: auto;
        margin-bottom: auto;
      }
    `;
  }

  render() {
    return html`
      <div class="container" id="accountsPanel">
        <account-list id="accountList"
          @selectedaccountchange="${this._selectedAccountChanged}">
        </account-list>
        <account-detail id="accountDetail"
          @newtransaction="${this._refreshAccountList}">
        </account-detail>
      </div>
    `;
  }
  static get properties() {
    return {
    };
  }

  _onNewContext(newContext){
    // propagate context change to children
    this.$('#accountList').context=this.context;
    this.$('#accountDetail').context=this.context;
  };

  _selectedAccountChanged(e){
    console.log("_selectedAccountChanged");
    this.resetMsgs();
    this.$('#accountDetail').activeAccount=e.detail.selectedAccount;
  }

  _refreshAccountList(e){
    console.log("_refreshAccountList");
    this.$('#accountList').refresh();
  }

}

window.customElements.define('accounts-panel', AccountsPanel);
