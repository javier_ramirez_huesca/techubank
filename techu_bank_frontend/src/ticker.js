import { LitElement, html, css } from 'lit-element';

class Ticker extends LitElement {

  static get styles() {
    return css`

    @-webkit-keyframes ticker {
      0% {
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
        visibility: visible;
      }
      100% {
        -webkit-transform: translate3d(-100%, 0, 0);
        transform: translate3d(-100%, 0, 0);
      }
    }
    @keyframes ticker {
      0% {
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
        visibility: visible;
      }
      100% {
        -webkit-transform: translate3d(-100%, 0, 0);
        transform: translate3d(-100%, 0, 0);
      }
    }

    .ticker-wrap {
      width: 100%;
      overflow: hidden;
      background-color: rgba(0, 0, 0, 0.9);
      box-sizing: content-box;
    }

    .ticker-wrap .ticker {
      display: inline-block;
      white-space: nowrap;
      box-sizing: content-box;
      -webkit-animation-iteration-count: infinite;
      animation-iteration-count: infinite;
      -webkit-animation-timing-function: linear;
      animation-timing-function: linear;
      -webkit-animation-name: ticker;
      animation-name: ticker;
      -webkit-animation-duration: 100s;
      animation-duration: 100s;
    }

    .ticker-wrap .ticker__item {
      display: inline-block;
      color: white;
      font-weight:bold;
      font-family:monospace;
    }

    .ticker-arrow-up{
    	color:green;
    }

    .ticker-arrow-down{
    	color:red;
    }

    @media (min-width: 768px){ /* IPad */
      .ticker-wrap .ticker__item {
        font-size:1.5rem;
      }
    }

    `;
  }

  render(){
    return html`
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <div class="ticker-wrap">
      <div class="ticker">
        ${this.mostActiveStockCompanies.map(i => html`
          <div class="ticker__item">
            <span title="${i.companyName}">${i.Ticker}</span>
            <!-- ticker icon -->
            ${i.Changes>0?
              html`<i class="ticker-arrow-up fas fa-caret-up"></i>`:html`<i class="ticker-arrow-down fas fa-caret-down"></i>`
            }
            <!-- ticker icon -->
            <span>${i.Price}</span>
            <span>${i.Changes}</span>
            <span>(${i.ChangesPerc.substring(0,8)}%)</span>
          </div>
          `)}

      </div>
    </div>
    `;
  }

  static get properties() {
    return {
      active: { type: Boolean , attribute:false }
    };
  }

  constructor() {
    super();
    this.mostActiveStockCompanies = [];
    this._gatherInfo();

  }

  _gatherInfo(){
    let elm = this;
    let xhr = new XMLHttpRequest();
    xhr.open('GET', "https://financialmodelingprep.com/api/stock/actives?datatype=json", true);
    xhr.onreadystatechange=function(){
      if (this.readyState == 4) {
        let resp = {};
        if(this.responseText!=null && this.responseText!=''){
          resp = JSON.parse(this.responseText);
        }
        if(this.status == 200 || this.status == 201){
          console.log("info from financialmodelingprep gathered");
          console.log(resp);
          // resp is an object with a property for each companyName
          // get only properties values to transform to an array
          elm.mostActiveStockCompanies = Object.values(resp);
          console.log(elm.mostActiveStockCompanies);
          elm.requestUpdate();
        }else{
          console.log("Error invoking callback:");
        }
      }
    };
    xhr.send(null);
  }

}
// Register the new element with the browser.
customElements.define('techu-ticker', Ticker);
