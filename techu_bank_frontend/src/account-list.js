import {html, css } from 'lit-element';
import {TechUBaseElement} from './base-component.js';
import './create-account-dialog.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-sort-column.js';


class AccountList extends TechUBaseElement {

  static get styles() {
    return css`

        .accounts-header{
          border-bottom:3px solid;
          margin-bottom: 0.5rem;
        }

        .create-account-bar{
          padding-right:0px!important;
          margin-bottom: 0.5rem;
        }

        .create-account-bar .btn-outline-primary{
          background-color:darkblue;
          color:white;
          margin-right: 0.1rem;
        }

        .alias{
          font-size:0.7rem;
          background-color:lightblue;
          color:darkblue;
          padding:0.3em;
          font-weight: bold;
        }

        @media (min-width: 768px){ /* IPad */
          .accounts-header h2{
            font-size:3rem;
          }

          .create-account-bar .btn-outline-primary{
            font-size:1.5rem;
          }

          .alias{
            font-size:1rem;
          }
        }

        @media (min-width: 1024px){ /* IPad Pro (laptops) */
          .accounts-header h2{
            font-size:4rem;
          }
        }
    `;
  }

  render(){
    return html`
    ${this.commonStyleSheets}
    <div class="container">
      <div class="row">
        <div class="col-12 d-flex justify-content-center accounts-header">
          <h2>accounts</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-12 d-flex justify-content-end create-account-bar">
          <button type="button" @click="${this._openAccDlg}" class="btn btn-outline-primary">Create Account</button>
          <create-account-dialog id="accDlg"
            @newaccount="${this.refresh}">
          </create-account-dialog>
        </div>
      </div>
      <div class="row">
        <vaadin-grid id="accountGrid"
          .items="${this.accounts}"
          height-by-rows
          @active-item-changed="${this._activeItemChanged}"
          theme="row-stripes"
          >
          <vaadin-grid-sort-column path="iban" header="IBAN" .renderer="${this._ibanRenderer}" width="70%">
          </vaadin-grid-sort-column>
          <vaadin-grid-sort-column path="balance" header="Balance" .renderer="${this._balanceRenderer}"
            text-align="end" width="30%">
          </vaadin-grid-sort-column>
        </vaadin-grid>
      </div>
    </div>
    `;
  }

  static get properties() {
    return {
      accounts:{type: Array,atribute:false},
      activeAccount:{type:Object,atribute:false}
    };
  }

  constructor() {
    super();
    this.accounts=[];
    this.activeAccount=null;
  }

  _onNewContext(newContext){
    console.log("onNewContext");
    console.log(this);
    this.$('#accDlg').context=newContext;
    this._gatherAccounts();
  }

  refresh(){
    this._gatherAccounts();
  }

  _ibanRenderer(root, column, rowData){
    let alias = (rowData.item.alias!=null)?'<br class="only-sm"/><span class="alias">'+rowData.item.alias+'</span>':"";
    root.innerHTML='<span class="iban">'+rowData.item.iban+' </span>'+alias;
  }

  _balanceRenderer(root, column, rowData){
    let style = (rowData.item.balance>=0)?'pos-balance':'neg-balance';
    root.innerHTML='<span class="'+style+'">'
                  +rowData.item.balance.toLocaleString(window.language,{"style":"currency","currency":"EUR"})
                  +'</span>';
  }

  _gatherAccounts(){
    console.log("_gatherAccounts");
    this.resetMsgs();
    this.bc.go('GET','techubank/v1/users/'+this._context.loggedUser+"/accounts",null,
        this._handleSucessfulGETAccounts.bind(this),
        this._handleError.bind(this));
  }

  _handleSucessfulGETAccounts(resp){
    console.log("_handleSucessfulGET");
    // add formated IBAN to returned objects
    resp.forEach(function(elm){
      elm.iban=elm._id.replace(/(.{4})(?!$)/g,"$1-");
    })
    this.accounts=resp;
    //this.$('#accountGrid').items=resp;
  }

  _activeItemChanged(event){
    console.log("_activeItemChanged");
    const item = event.detail.value;
    console.log(item);
    if(item!=null){
      // emit event for selected account change
      this.dispatchEvent(
        new CustomEvent(
        	"selectedaccountchange",
        	{
        	  "detail":{
              "selectedAccount":item
        	  }
        	}
      ));
    }
  }

  _openAccDlg(e){
    this.resetMsgs();
    this.$('#accDlg').open();
  }


}
customElements.define('account-list', AccountList);
