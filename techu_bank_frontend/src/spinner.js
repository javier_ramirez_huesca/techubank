// Import the LitElement base class and html helper function
import { LitElement, html, css } from 'lit-element';

// Extend the LitElement base class
class Spinner extends LitElement {

  static get styles() {
    return css`

    .spinner {
      position: relative;
      border: 0.4rem solid #f3f3f3;
      border-radius: 50%;
      border-top: 0.4rem solid #3498db;
      width: 3rem;
      height: 3rem;
      left:43%;
      top:45%;


      -webkit-animation: spin 2s linear infinite; /* Safari */
      animation: spin 2s linear infinite;
    }

    #overlay{
        position: absolute;
        top:0px;
        left:0px;
        width: 100%;
        height: 100%;
        background: black;
        opacity: .5;
    }

    /* Safari */
    @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }

    @media (min-width: 576px){
      .spinner {
        position: relative;
        border: 0.4rem solid #f3f3f3;
        border-radius: 50%;
        border-top: 0.4rem solid #3498db;
        width: 3rem;
        height: 3rem;
        left:50%;
        top:50%;
    }
    `;
  }

  render(){
    return html`
      <div id="overlay" style="display:none">
          <div class="spinner"></div>
      </div>
    `;
  }

  static get properties() {
    return {
      active: { type: Boolean , attribute:false }
    };
  }

  set active(v){
    let overlay = this.shadowRoot.getElementById('overlay');
    if(overlay!=null){
      if(v){
        overlay.style.display="block";
      }else{
        overlay.style.display="none";
      }
    }
  }

  constructor() {
    super();
    this.active = false;
  }

}
customElements.define('techu-spinner', Spinner);
