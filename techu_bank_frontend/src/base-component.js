import { LitElement, html, css } from 'lit-element';
import {BackEndConnector} from './backend-connector.js';

export class TechUBaseElement extends LitElement {

  static get styles() {
    return css`
    `;
  }

  render(){
    return html`
    `;
  }

  static get properties() {
    return {
      //context:{type:Object,attribute:false},
      msg:{type:String,attribute:false},
      errorMsg:{type:String,attribute:false}
    };
  }

  constructor() {
    super();
    this._context=null;
    this.bc = new BackEndConnector(this);
    this.resetMsgs();
  }

  resetMsgs(){
    this.msg=null;
    this.errorMsg=null;
  }

  get context(){
    return this._context;
  }

  set context(newContext){
    this._context=newContext;
    if(newContext!=null && newContext!=''){
      this.bc.jwt = newContext.jwt;
      this._onNewContext(newContext);
    }
  }

  /**
   * this function will be invoked when a non null new context were set
   */
  _onNewContext(newContext){};

  _handleError(errStatus,error){
    console.log("Error");
    console.log(error);
    this.errorMsg=error.message;
  }

  $(selector) {
    return this.shadowRoot.querySelector(selector);
  }

  get commonStyleSheets(){
    return html`
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="styles.css">
    `;
  }

}
