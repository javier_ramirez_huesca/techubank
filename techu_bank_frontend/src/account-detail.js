import {html, css } from 'lit-element';
import {TechUBaseElement} from './base-component.js';
import './tx-dialog.js';
import './stock-dialog.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-sort-column.js';


class AccountDetail extends TechUBaseElement {

  static get styles() {
    return css`

        #accountDetailPanel .selected-account-header{
            border-top: 3px solid;
            margin-top: 0.5rem
        }

        #accountDetailPanel .selected-account-header h3 {
          font-size:1.1rem;
          padding-top:1rem;
          font-weight: bold;
        }

        #accountDetailPanel .selected-account-alias{
          padding-bottom:0.2rem;
          margin-bottom:0.5rem;
        }

        #accountDetailPanel .selected-account-alias span{
          font-size:1rem;
          background-color:lightblue;
          padding-right:0.3rem;
          padding-left:0.3rem;
          font-weight: bold;
        }

        .tx-additionalInfo{
          font-size:0.7rem;
          color: gray;
        }

        .small-toolbar{
          border-top:2px solid lightblue;
          border-bottom:2px solid lightblue;
          padding-top:0.2rem;
          padding-bottom:0.2rem;
        }

        .small-toolbar button{
          border:0px;
          background-color:white;
        }

        .small-toolbar img{
          height:3rem;
          width:3rem;
        }

        .large-toolbar{
          display:none;
        }

        @media (min-width: 400px){ /* Nexus 5 */
          #accountDetailPanel .selected-account-header h3 {
            font-size:1.4rem;
          }
        }

        @media (min-width: 768px){ /* IPad */

          #accountDetailPanel .selected-account-header h3 {
            font-size:2.4rem;
          }

          #accountDetailPanel .selected-account-alias span{
            font-size:1.5rem;
          }

          .small-toolbar img{
            height:5rem;
            width:5rem;
          }

          .last-tx-header h4{
            font-size:2.2rem;
          }

          .tx-additionalInfo {
              font-size: 1.2rem;
          }

        }


        @media (min-width: 1024px){ /* IPad Pro (laptops) */
          .tx-additionalInfo {
            font-size:1rem;
          }

          #accountDetailPanel .selected-account-header h3 {
            font-size:2.7rem;
          }

          #accountDetailPanel .selected-account-alias span{
            font-size:2rem;
            padding-left:0.5rem;
            padding-right:0.5rem;
          }

          #accountDetailPanel .last-tx-header h4{
            font-size:2.4rem;
          }

          .large-toolbar{
            display:block;
          }

          .large-toolbar .btn{
            font-size:1.5rem;
            background-color:blue;
            color:white;
            margin: 0.1rem;
          }

          .small-toolbar{
            display:none;
          }

        }

    `;
  }

  render(){
    return html`
    ${this.commonStyleSheets}
    <!-- Account detail -->
    <div id="accountDetailPanel" style="display:none" class="container">
      <tx-dialog id="txDlg"
        @newtransaction="${this._refreshTxs}">
      </tx-dialog>
      <stock-dialog id="stockDlg"
        @newtransaction="${this._refreshTxs}">
      </stock-dialog>
        <div class="row">
          <div class="col-12 d-flex justify-content-center selected-account-header">
            <h3>${this.activeAccount.iban}</h3>
          </div>
        </div>
        <div class="row selected-account-alias">
          <div class="col-12 d-flex justify-content-center">
            <span>${this.activeAccount.alias}</span>
          </div>
        </div>
        <div class="large-toolbar">
          <div class="row">
            <div class="col-12 d-flex justify-content-end">
              <button id="withdrawBtn" type="button" @click="${this._openTxDlg}" class="btn btn-outline-primary">Withdrawal</button>
              <button id="depositBtn" type="button" @click="${this._openTxDlg}" class="btn btn-outline-primary">Deposit</button>
              <button id="transferBtn" type="button" @click="${this._openTxDlg}" class="btn btn-outline-primary">Transfer</button>
              <button id="stockBtn" type="button" @click="${this._openStockDlg}" class="btn btn-outline-primary">Stock</button>
            </div>
          </div>
        </div>
        <div class="small-toolbar">
          <div class="row">
            <div class="col-12 d-flex justify-content-between">
              <button id="withdrawBtn" type="button" @click="${this._openTxDlg}">
                <img src="img/withdrawal.png"/>
              </button>
              <button id="depositBtn" type="button" @click="${this._openTxDlg}">
                <img src="img/deposit.png"/>
              </button>
              <button id="transferBtn" type="button" @click="${this._openTxDlg}">
                <img src="img/transfer.png"/>
              </button>
              <button id="stockBtn" type="button" @click="${this._openStockDlg}">
                <img src="img/stock.png"/>
              </button>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12 d-flex justify-content-center last-tx-header">
            <h4>last transactions</h4>
          </div>
        </div>
        <div class="row">
          <vaadin-grid id="txsGrid"
            .items="${this.transactions}"
            height-by-rows theme="row-stripes">
            <vaadin-grid-sort-column path="timestamp" header="Date/Concept"
                .renderer="${this._dateConceptRenderer}"
                width="70%">
            </vaadin-grid-sort-column>
            <vaadin-grid-sort-column path="amount" header="Amount" text-align="end"
                .renderer="${this._amountRenderer}"
                width="30%">
            </vaadin-grid-sort-column>
          </vaadin-grid>
        </div>
    </div>
    `;
  }

  static get properties() {
    return {
      activeAccount:{type:Object,attribute:false},
      transactions:{type:Array,attribute:false}
    };
  }

  constructor() {
    super();
    this._reset();
  }

  _reset(){
    this._activeAccount={};
    this.transactions=[];
    this.resetMsgs();
  }

  _onNewContext(newContext){
    this.$('#txDlg').context=newContext;
    this.$('#stockDlg').context=newContext;
    this._reset();
  }

  get activeAccount(){return this._activeAccount;}

  set activeAccount(newActiveAccount){
    this._activeAccount=newActiveAccount;
    if(newActiveAccount!=null){
      // propagate account change to children
      this.$('#txDlg').activeAccountID=this._activeAccount._id;
      this.$('#stockDlg').activeAccountID=this._activeAccount._id;
      // refresh tx information
      this._refreshTxs();
      // show detail panel
      this.$('#accountDetailPanel').style.display='block';
    }
  }

  _refreshTxs(){
    this.bc.go('GET',"techubank/v1/users/"+this._context.loggedUser+"/accounts/"+this.activeAccount._id+"/transactions",null,
              this._handleSucessfulGETTransactions.bind(this),
              this._handleError.bind(this));
  }

  _handleSucessfulGETTransactions(resp){
    this.transactions=this._buildTxsRows(resp);

  }

  _buildTxsRows(txs){
    console.log(txs);
    let rows = [];

    for(let i=0;i<txs.length;i++){
      let concept = "transfer";
      if(txs[i].source==null){concept="deposit";}
      if(txs[i].target==null){concept="withdraw";}

      let additionalInfo;

      if(concept=="transfer"){ // transfer
        if(txs[i].source==this.activeAccount._id){
          additionalInfo="(to "+this._formatIBAN(txs[i].target)+")";
        }
        else if(txs[i].target==this.activeAccount._id){
          additionalInfo="(from "+this._formatIBAN(txs[i].source)+")";
        }

      }

      let amount = txs[i].amount;
      if(txs[i].source==this.activeAccount._id){
        amount = -(amount);
      }

      rows[i] = {
        "timestamp":txs[i].timestamp,
        "concept":concept,
        "additionalInfo":additionalInfo,
        "amount":amount}
    }
    return rows;
  }

  _formatIBAN(id){
    return id.replace(/(.{4})(?!$)/g,"$1-");
  }

  _dateConceptRenderer(root, column, rowData){
    let date = new Date(rowData.item.timestamp);
    let ih ='<span class="tx-date">'
              +date.toLocaleString()
              +' </span>'
              +'<span class="tx-'+rowData.item.concept+'">'
              +rowData.item.concept
              +' </span>';

    if(rowData.item.additionalInfo!=null){
        ih += '<br class="only-sm"/>'
              +'<span class="tx-additionalInfo">'
              +rowData.item.additionalInfo
              +'</span>';
    }
    root.innerHTML = ih;
  }

  _amountRenderer(root, column, rowData){
    let style = 'pos-balance';
    if(rowData.item.amount<0){
      style = 'neg-balance';
    }

    root.innerHTML='<span class="'+style+'">'
        +rowData.item.amount.toLocaleString(window.language,{"style":"currency","currency":"EUR"})
        +'</span>';
  }


  _openTxDlg(e){
    this.resetMsgs();
    let btn = e.path[0];
    if(btn.nodeName=='IMG') btn = e.path[1];
    let btnID = btn.id;

    let txDlg = this.$('#txDlg');

    if(btnID=='withdrawBtn'){
      txDlg.txMode='withdraw';
    }else if(btnID=='depositBtn'){
      txDlg.txMode='deposit';
    }else{
      txDlg.txMode='transfer';
    }
    txDlg.open();
  }

  _openStockDlg(e){
    this.resetMsgs();
    this.$('#stockDlg').open();
  }



}
customElements.define('account-detail', AccountDetail);
