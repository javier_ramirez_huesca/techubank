const UNK_ERR = {"code":"UNKNOWN","message":"Unknown error"};

export class BackEndConnector{
  constructor(elm){
    // create request-json client for back-end
    //this.backEndClient = requestJson.createClient(backEndURL);
    this.backEndURL = "http://localhost:3000/";
    this.req = new XMLHttpRequest();
    this.jwt = null;
    this.elm = elm;
  }

  go(method,relativeURL,body,callback,errorCallback){
    this.req.open(method, this.backEndURL+relativeURL, true);
    this.req.onreadystatechange = this.wrapperCallback(callback,errorCallback);
    this.req.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    if(this.jwt!=null){
      this.req.setRequestHeader("Authorization","Bearer "+this.jwt);
    }
    this._emitStartLoadingEvent();
    this.req.send(JSON.stringify(body));
  }

  // Method returning a wrappering callback function to manage errors in a common way
  wrapperCallback(callback,errorCallback){
    let bc = this;
    return function(){
      if (this.readyState == 4) {
        bc._emitEndLoadingEvent();
        let resp = {};
        if(this.responseText!=null && this.responseText!=''){
          // assume response is a json
          resp = JSON.parse(this.responseText);
        }
        //resp.status = this.status;
        if(this.status == 200 || this.status == 201){
          callback(resp);
        }else{
          console.log("Error invoking callback:");
          let error = UNK_ERR;
          if(resp.error!=null) error = resp.error;
          console.log(error);
          errorCallback(this.status, error);
        }
      }
    };
  }

  _emitStartLoadingEvent(){
    console.log("_emitStartLoadingEvent");
    this.elm.dispatchEvent(
      new CustomEvent(
    	"startloading",
    	{
        detail:{},
        bubbles: true,
        composed: true
      }
    ));
  }

  _emitEndLoadingEvent(elm){
    console.log("_emitEndLoadingEvent");
    this.elm.dispatchEvent(
      new CustomEvent(
    	"endloading",
    	{
        detail:{},
        bubbles: true,
        composed: true
    	}
    ));
  }

}
