import { LitElement, html, css } from 'lit-element';
import './user-login.js';
import './accounts-panel.js';
import './spinner.js';


class MainPanel extends LitElement {
  constructor(){
    super();
    this.context=null;

  }

  render() {
    return html`
      <user-login id="login"
          @loginevent="${this._broadcastLoginEvent}"
          @logoutevent="${this._broadcastLogoutEvent}"
          @startloading="${this._activateSpinner}"
          @endloading="${this._deactivateSpinner}">
      </user-login>
      <div id="mainPanel" style="display:none">
        <accounts-panel id="accountsPanel"
          @startloading="${this._activateSpinner}"
          @endloading="${this._deactivateSpinner}">
        </accounts-panel>
      </div>
      <techu-spinner id="spinner"></techu-spinner>

    `;
  }
  static get properties() {
    return {
      "context":{type:Object}
    };
  }

  $(selector) {
    return this.shadowRoot.querySelector(selector);
  }


  _broadcastLoginEvent(event){
    console.log("LoginEvent");
    //console.log(document.getElementByTagName("mainPanel"));
    this.context=event.detail;
    this.$('#accountsPanel').context=this.context;
    this.$('#mainPanel').style.display="block";
  }

  _broadcastLogoutEvent(event){
    console.log("LogoutEvent");
    this.context=null;
    this.$('#accountsPanel').context=null;
    this.$('#mainPanel').style.display="none";
  }

  _activateSpinner(){
    console.log("_activateSpinner");
    this.$('#spinner').active=true;
  }

  _deactivateSpinner(){
    console.log("_deactivateSpinner");
    this.$('#spinner').active=false;
  }

}

window.customElements.define('main-panel', MainPanel);
