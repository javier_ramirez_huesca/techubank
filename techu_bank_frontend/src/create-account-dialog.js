import {html, css } from 'lit-element';
import {TechUBaseElement} from './base-component.js';
import '@polymer/paper-dialog/paper-dialog.js';

class CreateAccountDialog extends TechUBaseElement {

  static get styles() {
    return css`

        @media (min-width: 768px){ /* IPad */
        }
    `;
  }

  render(){
    return html`
    ${this.commonStyleSheets}
    <!-- CREATE ACCOUNT DIALOG -->
    <paper-dialog id="accDlg" modal>
      <div class="dialog-body">
        <h2 class="">Do you want give account a name?</h2>
        <div class="form-group row">
          <label for="accountAlias" class="col-12  col-form-label">Name: (f.i. expenses, optional)</label>
          <div class="col-12">
                <input id="accountAlias" type="text" class="form-control" placeholder="name (optional)"
                  .value="${this.alias}" @change="${(e)=>this.alias=e.target.value}"/>
          </div>
        </div>
      </div>
      <div class="d-flex justify-content-between dlg-buttons">
        <button dialog-dismiss class="btn btn-sm">Cancel</button>
        <button autofocus @click="${this._createAccount}" class="btn btn-sm">Accept</button>
      </div>
    </paper-dialog>
    <!-- CREATE ACCOUNT DIALOG -->
    `;
  }

  static get properties() {
    return {
      alias:{type:String,atribute:false}
    };
  }

  constructor() {
    super();
    this.alias="";
  }

  open(){
    this.$('#accDlg').open();
  }

  _createAccount(event){
    console.log("_createAccount");
    // build account and post
    let newAcc = {};
    if(this.alias!=null && this.alias.trim().length>0){
      newAcc.alias=this.alias;
    }
    this.$('#accDlg').close();
    this.bc.go('POST',"techubank/v1/users/"+this.context.loggedUser+"/accounts",newAcc,
                this._handleSucessfulPOST.bind(this),
                this._handleError.bind(this));
  }

  _handleSucessfulPOST(resp){
    this.msg=resp.message;
    // emit event for accounts info modified
    this.dispatchEvent(
      new CustomEvent(
      	'newaccount',
      	{
      	  detail:{
            newAccount:this.msg
      	  },
          bubbles: true,
          composed: true
      	}
    ));
  }

}
customElements.define('create-account-dialog', CreateAccountDialog);
