import {html, css } from 'lit-element';
import {TechUBaseElement} from './base-component.js';
import '@polymer/paper-dialog/paper-dialog.js';

class TxDialog extends TechUBaseElement {

  static get styles() {
    return css`
    `;
  }

  render(){
    return html`
    ${this.commonStyleSheets}
    <!-- TX DIALOG -->
    <paper-dialog id="txDlg" modal>
      <div class="dialog-body">
        <h2>${this.txMode} amount?</h2>
        <div class="form-group row" id="target">
          <label for="targetAccount" class="col-5  col-form-label">IBAN</label>
          <div class="col-12">
                <input id="targetAccount" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group row">
          <label for="amount" class="col-sm-3 col-form-label">Amount</label>
          <div class="col-12">
                <input id="amount" type="text" class="form-control"/>
          </div>
        </div>
      </div>
      <div class="d-flex justify-content-between dlg-buttons">
        <button dialog-dismiss class="btn btn-sm">Cancel</button>
        <button autofocus @click="${this._doTx}" class="btn btn-sm">Accept</button>
      </div>
    </paper-dialog>
    <!-- TX DIALOG -->
    `;
  }

  static get properties() {
    return {
      activeAccountID:{type:String,attribute:false},
      txMode:{type:String,attribute:false}
    };
  }

  constructor() {
    super();
    this.activeAccountID=null;
    this.txMode=null;
  }

  open(){
    if(this.txMode!='transfer'){
      this.$('#target').style.display='none';
    }else{
      this.$('#target').style.display='block';
    }
    this.$('#txDlg').open();
  }

  _doTx(){
    // create a transaction without target and source selected account
    console.log("_doTX");
    let newTX = {
      "source":this.activeAccountID,
      "amount":parseFloat(this.$('#amount').value,10)
    };
    if(this.txMode=='deposit'){
      newTX.target=newTX.source;
      newTX.source=null;
    }else if(this.txMode=='transfer'){
      newTX.target=this.$('#targetAccount').value.replace(/-/g,''); // remove '-'
    }
    this.$('#txDlg').close();
    this.bc.go('POST',"techubank/v1/users/"+this.context.loggedUser+"/accounts/"+this.activeAccountID+"/transactions",newTX,
              this._handleSucessfulPOSTTransaction.bind(this),
              this._handleError.bind(this));
  }

  _handleSucessfulPOSTTransaction(resp){
    console.log("_handleSucessfulPOSTTransaction");
    // emit event for accounts info modified (not only tx)
    this.dispatchEvent(
      new CustomEvent(
      	'newtransaction',
      	{
      	  detail:{},
          bubbles: true,
          composed: true
      	}
    ));
  }


}
customElements.define('tx-dialog', TxDialog);
