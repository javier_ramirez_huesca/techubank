const JWT_ERROR = "JWT_ERROR";
const BEARER_TOKEN_PREFIX = "Bearer ";
const jwt = require('jsonwebtoken');

class TechUException extends Error{
  constructor(code, ...params) {
    // Pass remaining arguments (including vendor specific ones) to parent constructor
    super(...params);
    this.name = 'TechUException';
    // Custom debugging information
    this.code = code;
  }
}

function handleException(exc,req,res){
  console.log(exc);
  console.log((exc instanceof Error));
  console.log((exc instanceof TechUException));
  if(exc instanceof TechUException){
      if(exc.code==JWT_ERROR){
        res.status(401).send(error("INVALID_JWT",(exc.message!=null&&exc.message!='')?exc.message:"No JWT (or invalid) found"));
      }else{
          res.status(500).send(error(exc.code,null));
      }
      return;
  }
  res.status(500).send({"puff":"q mal"});
}

/*
* Build an error object to be returned on API responses
*/
function error(code,msg){
  return {
    "error":{
      "code":code,
      "message":msg
    }
  }
}

/*
* Build an error object to be returned on API responses
*/
function okRes(msg){
  return {
    "message":msg
  }
}

function validateJWT(req,res,user){
  // gather JWT Headers
  console.log(req.headers);
  if(req.headers.authorization!=null){
    if(req.headers.authorization.startsWith(BEARER_TOKEN_PREFIX)){
      let token = req.headers.authorization.substring(BEARER_TOKEN_PREFIX.length);
      console.log(token);
      try{
        let decoded_token = jwt.verify(token,process.env.JWT_SIGN_KEY);
        console.log(decoded_token);
        if(user !=null && user!=decoded_token.user){
          throw new Error("User is not authorized to perform operation");
        }else{
          return decoded_token;
        }
      }catch(exc){
        throw new TechUException(JWT_ERROR,exc.message);
      }
    }
  }
  throw new TechUException(JWT_ERROR);
}

module.exports.error=error;
module.exports.okRes=okRes;
module.exports.validateJWT=validateJWT;
module.exports.handleException=handleException;
