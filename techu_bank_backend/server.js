// required for managing environment configuration
require('dotenv').config();

// import 'express' for HTTP server manangement
const express = require('express');

// init express framework
const app = express();

// use JSON as default contenty-type
app.use(express.json());

// Enable CORS
// TODO review config
var enableCORS = function(req, res, next){
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
  res.set("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
}
app.use(enableCORS);


// init port from env config
const port = process.env.PORT

if(port==null){
  // no config available -> exit
  console.log("No config found por listening port. Please check configuration (PORT)");
  return;
}else{
  console.log("Opening port "+port+" for listen requests ...");
  app.listen(port);
  console.log("Port "+port+" opened");

  // register REST API paths to corresponding functions
  const userController = require('./controllers/UserController.js');

  app.post("/techubank/v1/users",userController.registerUserV1);
  app.post("/techubank/v1/sessions",userController.loginUserV1);

  const accountController = require('./controllers/AccountController.js');

  app.post("/techubank/v1/users/:userID/accounts",accountController.createAccountV1);
  app.get("/techubank/v1/users/:userID/accounts",accountController.getUserAccountsV1);

  const transactionController = require('./controllers/TransactionController.js');

  app.post("/techubank/v1/users/:userID/accounts/:accountID/transactions",transactionController.createTransactionV1);
  app.get("/techubank/v1/users/:userID/accounts/:accountID/transactions",transactionController.getAccountTransactionsV1);


}
