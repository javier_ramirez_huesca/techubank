const BaseMongoPersistenceManager = require('./BaseMongoPersistenceManager.js');

class CounterPersistenceManager extends BaseMongoPersistenceManager{
  constructor(){
    super(null);
  }

  // method for increase and get counter
  increaseCounter(counter,callback/*(err,bodyRes)*/){
    // TODO validate user information before sending it to mongo
    let body = {
      	"findAndModify":"counters",
          "query": {
              "_id": counter
          },
          "update": {
              "$inc": {
                  "sequence_value": 1
              }
          },
          "new":true
    };
    super.post(null,body,callback);
  }

  getUserByIdV1(userID, callback){
    super.get(['q={"_id":"'+userID+'"}'],callback);
  }

}

module.exports=CounterPersistenceManager;
