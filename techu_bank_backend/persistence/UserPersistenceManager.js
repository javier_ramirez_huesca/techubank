const BaseMongoPersistenceManager = require('./BaseMongoPersistenceManager.js');
console.log("BaseMongoPersistenceManager");
console.log(BaseMongoPersistenceManager);
class UserPersistenceManager extends BaseMongoPersistenceManager{
  constructor(){
    super("users");
  }
  // method for inserting user in MLab
  insertUser(user,callback/*(err,bodyRes)*/){
    // TODO validate user information before sending it to mongo
    let mngUsr = {
      "_id":user.email,
      "firstName":user.firstName,
      "lastName":user.lastName,
      "password":user.password
    };
    super.post(null,mngUsr,callback);
  }

  getUserByIdV1(userID, callback){
    super.get(['q={"_id":"'+userID+'"}'],callback);
  }

}

module.exports=UserPersistenceManager;
