// import request-json for REST invokation to MLab
const requestJson = require('request-json');

// Base URL for MLab API
const baseMLabURL = "https://api.mlab.com/api/1/databases/"+process.env.MLAB_DB_NAME;
// apiKey parameter for authentication on MLab
const mLabAPIKey = "apiKey="+process.env.MLAB_API_KEY;

const RUN_COMMAND = "runCommand";

class BaseMongoPersistenceManager{
  constructor(collectionName){
    this.collectionName = collectionName;
    this.mlabClient = this.getMLabClient(collectionName);
    this.mlabCommnadClient = null;
  }

  /**
  * this method allows to create several clients
  */
  getMLabClient(collectionName){
    if(collectionName!=null){
      // create request-json client for MLab
      return requestJson.createClient(baseMLabURL+"/collections/"+this.collectionName+"/");
    }else{
      // create request-json client for MLab runCommand
      return requestJson.createClient(baseMLabURL+"/"+RUN_COMMAND);
    }
  }
  // get qury string
  addAuthenticationToQueryString(queryParams){
    return (queryParams==null)?("?"+mLabAPIKey):("?"+queryParams.join("&")+"&"+mLabAPIKey);
  }
  // method for post a request to MLab
  post(queryParams,body,callback){
    console.log('performing post to mongo');
    this.mlabClient.post(this.addAuthenticationToQueryString(queryParams),body,this.wrapperedCallback(callback));
  }
  // method for get a request to MLab
  get(queryParams,callback){
    console.log('performing get to mongo');
    this.mlabClient.get(this.addAuthenticationToQueryString(queryParams),this.wrapperedCallback(callback));
  }

  _initCommandClient(){
    if(this.mlabCommnadClient==null){
      this.mlabCommnadClient = this.getMLabClient(null);
    }
  }

  findAndModify(findAndModify,callback){
    this._initCommandClient();
    console.log('performing findAndModify to mongo');
    console.log(findAndModify);
    this.mlabCommnadClient.post(this.addAuthenticationToQueryString(null),findAndModify,
              this.wrapperedCallback(callback));
  }

  // Method returning a wrappering callback function to manage errors in a common way
  wrapperedCallback(callback){
    return function(){
      // in this function we can process errors before passing them to client
      // and hide complexity of MLab response object, not passing it to client
      // TODO manage errors
      console.log("res from mongo");
      console.log(arguments[2]);
      let error = arguments[0];
      if(error!=null){
        console.log("Error invoking MLab");
        console.log(error);
      }
      callback(error, arguments[2]/*mngRes*/);
    };
  }
}

module.exports=BaseMongoPersistenceManager;
