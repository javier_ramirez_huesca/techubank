const BaseMongoPersistenceManager = require('./BaseMongoPersistenceManager.js');

class AccountPersistenceManager extends BaseMongoPersistenceManager{
  constructor(){
    super("accounts");
  }
  // method for inserting account in MLab
  insertAccount(account,callback/*(err,bodyRes)*/){
    super.post(null,account,callback);
  }

  getUserAccounts(userID,callback){
    super.get(['q={"participants.id":"'+userID+'"}'],callback);
  }

  updateBalance(accountID,amount,callback){
    super.findAndModify({
      	"findAndModify":"accounts",
          "query": {
              "_id": accountID
          },
          "update": {
              "$inc": {
                  "balance": amount
              }
          },
          "new":true
    },callback);
  }

}

module.exports=AccountPersistenceManager;
