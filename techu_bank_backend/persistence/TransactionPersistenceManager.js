const BaseMongoPersistenceManager = require('./BaseMongoPersistenceManager.js');
const TX_LIMIT = 10;

class TransactionPersistenceManager extends BaseMongoPersistenceManager{
  constructor(){
    super("transactions");
  }
  // method for inserting account in MLab
  insertTransaction(transaction,callback/*(err,bodyRes)*/){
    super.post(null,transaction,callback);
  }

  getAccountTransacctions(accountID,callback){
    let query = 'q={$or:[{"source":"'+accountID+'"},{"target":"'+accountID+'"}]}';
    super.get([query,'s={"timestamp":-1}','l='+TX_LIMIT],callback);
  }

}

module.exports=TransactionPersistenceManager;
