const UserPersistenceManager = require('../persistence/UserPersistenceManager.js');
const crypt = require('../crypt.js');
const utils = require('../utils.js');
const jwt = require('jsonwebtoken');

let usrPM = new UserPersistenceManager();

function registerUserV1(req,res){
  let user = req.body;
  console.log("registerUserV1:("+req.method+","+req.route.path+","+user.email+")");
  // TODO validate complete input
  if(user.email==null || user.email.trim()==''){
    res.status(400).send(utils.error("INVALID_USER_INFO","Invalid user information"));
    return;
  }

  usrPM.getUserByIdV1(user.email,function(err,mngRes){
    console.log(mngRes);
    if(mngRes==null || mngRes.length==0){
      console.log("user not exists, create");
      // hash password before inserting
      user.password = crypt.hash(user.password);
      usrPM.insertUser(user,function(err,mngRes){
          if(!err){
            res.status(201).send(utils.okRes("user identified by "+user.email+" succesfully registered"));
          }else{
            res.status(500).send(utils.error("UNKNOWN",err));
          }
      });
    }else{
      // user account exists -> 409 (conflict)
      res.status(409).send(utils.error("INVALID_REG_EMAIL","Invalid user email"));
    }
  });
}

function _buildJWT(user){
  return jwt.sign({ user: user._id }, process.env.JWT_SIGN_KEY, {expiresIn:'1h'});
}

function loginUserV1(req,res){
  let user = req.body;
  console.log("loginUserV1:("+req.method+","+req.route.path+","+user.email+")");
  // get user to check password
  usrPM.getUserByIdV1(user.email,function(err,mngUsrList){
    console.log(mngUsrList);
    if(mngUsrList==null || mngUsrList.length==0){
      console.log("user not exists -> error");
      res.status(401).send(utils.error("INVALID_LOGIN","Invalid login data"));
    }else{
      // user account exists -> check password
      // hash password before inserting
      if(mngUsrList.length==1){
        if(crypt.checkPassword(user.password,mngUsrList[0].password)){
          // valid password -> create JWT token
          let mngUsr = mngUsrList[0];
          let jwt = _buildJWT(mngUsr);
          res.status(200).send({
                    "jwt":jwt,
                    "firstName":mngUsr.firstName,
                    "lastName":mngUsr.lastName
                  });
          return;
        }else{
          res.status(401).send(utils.error("INVALID_LOGIN","Invalid login data"));
        }
      }else{
        res.status(500).send(utils.error("UNKNOWN",err));
      }
    }
  });
}

module.exports.registerUserV1=registerUserV1;
module.exports.loginUserV1=loginUserV1;
