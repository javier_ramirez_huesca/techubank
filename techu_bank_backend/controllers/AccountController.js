const AccountPersistenceManager = require('../persistence/AccountPersistenceManager.js');
const CounterPersistenceManager = require('../persistence/CounterPersistenceManager.js');
const crypt = require('../crypt.js');
const utils = require('../utils.js');
const jwt = require('jsonwebtoken');
const IBAN = require('iban');
const ENTITY = process.env.ENTITY;
const BRANCH = process.env.BRANCH;

let cntPM = new CounterPersistenceManager();
let accPM = new AccountPersistenceManager();

function getControlDigit(str/*string[10]*/){
      str = str.padStart(10,"0");
      let intSum=0;
      for (let intI=0;intI<10;intI++)
          intSum+=(Math.pow(2,intI) * str[intI]);
      intSum%= 11;
      return (intSum<=1?intSum:(11-intSum));
}

function buildIBANforNewaccount(accountID /*int*/){
    // first CD came from entity and office (9999-0001)
    let firstDC = getControlDigit(ENTITY+BRANCH);
    // second CD came from account Number
    let paddedAccount = (new String(accountID)).padStart(10,"0");
    let secondDC = getControlDigit(paddedAccount);
    let bban = ENTITY+BRANCH+firstDC+secondDC+paddedAccount;
    let iban = IBAN.fromBBAN("ES",bban);
    return iban;
}


function createAccountV1(req,res){
  let userID = req.params.userID;
  console.log("createAccountV1:("+req.method+","+req.route.path+","+userID+")");
  try{
    utils.validateJWT(req,res,userID);
    let newAccount = {
      "participants": [{"id":userID,"role":"holder"}],
      "alias": req.body.alias,
      "creationDate" : new Date(),
      "balance": 0
    }
    // first get new account number
    cntPM.increaseCounter("accountID",function(error,resMongo){
      if(error==null){
        // new counter value is in value.sequence_value
        let newAccountID = resMongo.value.sequence_value;
        console.log("newAccountID:"+newAccountID);
        // calculate CCC for new account
        let iban = buildIBANforNewaccount(newAccountID);
        console.log(iban);
        newAccount._id=iban;
        // insert account
        accPM.insertAccount(newAccount,function(error,resMongo){
          if(error==null){
            res.status(201).send(utils.okRes("account identified by "+newAccount._id+" succesfully created"));
          }else{
            res.status(500).send(error);
          }
        });
      }else{
          // TODO handleError
          res.status(500).send(error);
      }

    });
  }catch(exc){
    utils.handleException(exc,req,res);
  }
}

function getUserAccountsV1(req,res){
  let userID = req.params.userID;
  console.log("getUserAccountsV1:("+req.method+","+req.route.path+","+userID+")");
  try{
    utils.validateJWT(req,res,userID);
    accPM.getUserAccounts(userID,function(error,resMongo){
      if(error==null){
        res.status(200).send(resMongo);
      }else{
        res.status(500).send(error);
      }
    });
  }catch(exc){
    utils.handleException(exc,req,res);
  }
}

module.exports.createAccountV1=createAccountV1;
module.exports.getUserAccountsV1=getUserAccountsV1;
