const TransactionPersistenceManager = require('../persistence/TransactionPersistenceManager.js');
const AccountPersistenceManager = require('../persistence/AccountPersistenceManager.js');
const utils = require('../utils.js');

let txPM = new TransactionPersistenceManager();
let accPM = new AccountPersistenceManager();

function createTransactionV1(req,res){
  let userID = req.params.userID;
  let accountID = req.params.accountID;
  console.log("createTransactionV1:("+req.method+","+req.route.path+","+userID+","+accountID+")");
  try{
    utils.validateJWT(req,res,userID);
    let newTransaction = {
      "source":req.body.source,
      "target":req.body.target,
      "amount":req.body.amount,
      "timestamp":new Date(),
      "sender": userID
    }
    if(isNaN(parseFloat(newTransaction.amount,10))){
      res.status(400).send(utils.error('INVALID_AMOUNT',"Transaction amount ("+newTransaction.amount+") is not a number"));
      return;
    }

    let pr = new Promise((resolve,reject)=>{ // first insert tx
      txPM.insertTransaction(newTransaction,(error,resMongo)=>{
        if(error==null){
          resolve(resMongo);
        }else{
          reject(error);
        }
      });
    });
    pr.then((success)=>{ // then add money to target (if apply)
      if(newTransaction.target!=null){
        return new Promise((resolve,reject)=>{
          accPM.updateBalance(newTransaction.target,newTransaction.amount,(error,resMongo)=>{
            if(error==null){
              resolve(resMongo);
            }else{
              reject(error);
            }
          });
        });
      }
    }).then((success)=>{ // then remove money from source (if apply)
        if(newTransaction.source!=null){
          return new Promise((resolve,reject)=>{
            accPM.updateBalance(newTransaction.source,-(newTransaction.amount),(error,resMongo)=>{
              if(error==null){
                resolve(resMongo);
              }else{
                reject(error);
              }
            });
          });
        }
    }).then((success)=>{
      res.status(201).send(utils.okRes("transaction succesfully created"));
    }).catch((reason)=>{
      console.log("catch");
      res.status(500).send(error);
    });
  }catch(exc){
    utils.handleException(exc,req,res);
  }
}

function getAccountTransactionsV1(req,res){
  let userID = req.params.userID;
  let accountID = req.params.accountID;
  console.log("getAccountTransactionsV1:("+req.method+","+req.route.path+","+userID+","+accountID+")");
  // TODO check user is account participant
  try{
    utils.validateJWT(req,res,userID);
    txPM.getAccountTransacctions(accountID,function(error,resMongo){
      if(error==null){
        res.status(200).send(resMongo);
      }else{
        res.status(500).send(error);
      }
    });
  }catch(exc){
    utils.handleException(exc,req,res);
  }
}

module.exports.createTransactionV1=createTransactionV1;
module.exports.getAccountTransactionsV1=getAccountTransactionsV1;
